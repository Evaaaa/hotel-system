package com.hotelSystem.Room;

import com.hotelSystem.Reservation.Reservation;

import java.util.List;

public class RoomDTO {
    public int id;
    public int numberOfPeople;
    public double price;
    public String type;
    public List<Reservation> reservations;

    public RoomDTO(int id, int numberOfPeople, double price, String type, List<Reservation> reservations) {
        this.id = id;
        this.numberOfPeople = numberOfPeople;
        this.price = price;
        this.type = type;
        this.reservations = reservations;
    }

}
