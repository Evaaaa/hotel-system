package com.hotelSystem.Room;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/rooms")
public class RoomResource {

        private final RoomService roomService;

        public RoomResource(RoomService roomService) {

            this.roomService = roomService;
        }


        @PostMapping(consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
        public RoomDTO create(@RequestBody RoomRequest request){
            return roomService.create(request);
        }

        @PutMapping(path = "/{roomId}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
        public RoomDTO update(@PathVariable("roomId") final Integer roomId, @RequestBody final RoomRequest request){
            return roomService.update(roomId,request);
        }
        @DeleteMapping(path = "/{roomId}",produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
        public ResponseEntity<HttpStatus> delete(@PathVariable("roomId") final Integer roomId){
            return roomService.delete(roomId);
        }
    }



