package com.hotelSystem.Room;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.hotelSystem.Reservation.Reservation;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "room")
public class Room {
    public int id;
    public int numberOfPeople;
    public double price;
    public String type;
    public List<Reservation> reservations;

    public Room(Integer id) {
    }

    public Room(int id, int numberOfPeople, double price, String type) {
        this.id = id;
        this.numberOfPeople = numberOfPeople;
        this.price = price;
        this.type = type;
        this.reservations = new ArrayList<>();
    }

    public Room(String name) {

    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Room{ id= ").append(id);
        sb.append(" ,numberOfPeople= ").append(numberOfPeople);
        sb.append(" ,price= ").append(price);
        sb.append(" ,type= ").append(type);
        sb.append(" ,reservations= [");
        if (reservations.size() > 0) {
            sb.append("\n");
            for (Reservation reservation : reservations) {
                sb.append("\t" + reservation.toString());
                sb.append("\n");
            }
        }

        sb.append("]}");
        return sb.toString();
    }

    @Override
    public int hashCode() {

        return Objects.hash(31);
    }

    public RoomDTO toDTO() {
        return new RoomDTO(id, numberOfPeople, price, type, reservations);

    }

    public int getId() {

        return id;
    }

    public String getType() {
        return type;
    }
}
