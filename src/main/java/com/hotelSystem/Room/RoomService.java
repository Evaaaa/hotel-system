package com.hotelSystem.Room;

import com.hotelSystem.Reservation.Reservation;
import com.hotelSystem.exception.ResourceValidationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.UUID;

public class RoomService {
    private final RoomRepository repository;

    public RoomService(RoomRepository repository) {
        this.repository = repository;
    }


    public RoomDTO create(final RoomRequest request) {
        var isPresentRoom = repository.findById(request.id).isPresent();
        if (isPresentRoom) {
            throw new RuntimeException("The room already exists.");
        }
        var room = new Room(request.id);
        return repository.save(room).toDTO();
    }

    @Transactional
    public RoomDTO update(final Integer roomId, final RoomRequest request) {
        var room = repository.findById(roomId)
                .orElseThrow(() -> new ResourceValidationException("Room with ID " + roomId + " is not found"));
       room.id = request.id;
        return repository.save(room).toDTO();
    }

    public ResponseEntity<HttpStatus> delete(final Integer roomId) {
        var room = repository.findById(roomId)
                .orElseThrow(() -> new ResourceValidationException("Room with ID " + roomId + " is not found"));
        repository.delete(room);
        return new ResponseEntity<>(HttpStatus.OK);
    }


    }




