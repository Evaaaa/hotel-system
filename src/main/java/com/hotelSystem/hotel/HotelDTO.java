package com.hotelSystem.hotel;

public class HotelDTO {
    public Long id;
    public String name;

    public HotelDTO(String name) {
        this.name = name;
    }

    public HotelDTO(Long id, String name) {
        this.id = id;
        this.name = name;
    }
}
