package com.hotelSystem.hotel;


import com.hotelSystem.Reservation.Reservation;
import com.hotelSystem.Room.Room;
import com.hotelSystem.exception.ResourceValidationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;

@Service

public class HotelService {

    private final HotelRepository repository;

    public HotelService(HotelRepository repository) {
        this.repository = repository;
    }


    public HotelDTO create(final HotelRequest request) {
        var isPresentHotel = repository.findByName(request.name).isPresent();
        if (isPresentHotel) {
            throw new RuntimeException("The hotel already exists.");
        }
        var hotel = new Hotel(request.name);
        return repository.save(hotel).toDTO();
    }

    @Transactional
    public HotelDTO update(final String hotelId, final HotelRequest request) {
        var hotel = repository.findByName(hotelId)
                .orElseThrow(() -> new ResourceValidationException("Hotel with ID " + hotelId + " is not found"));
        hotel.name = request.name;
        return repository.save(hotel).toDTO();
    }

    public ResponseEntity<HttpStatus> delete(final String hotelId) {
        var hotel = repository.findByName(hotelId)
                .orElseThrow(() -> new ResourceValidationException("Hotel with ID " + hotelId + " is not found"));
        repository.delete(hotel);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}

