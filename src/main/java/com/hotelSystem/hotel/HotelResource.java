package com.hotelSystem.hotel;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/hotels")
public class HotelResource {

    private final HotelService hotelService;

    public HotelResource(HotelService hotelService) {
        this.hotelService = hotelService;
    }


    @PostMapping(consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public HotelDTO create(@RequestBody HotelRequest request){
        return hotelService.create(request);
    }

    @PutMapping(path = "/{hotelId}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public HotelDTO update(@PathVariable("hotelId") final String hotelId, @RequestBody final HotelRequest request){
        return hotelService.update(hotelId,request);
    }
    @DeleteMapping(path = "/{hotelId}",produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ResponseEntity<HttpStatus> delete(@PathVariable("hotelId") final String hotelId){
        return hotelService.delete(hotelId);
    }
}

