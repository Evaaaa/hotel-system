package com.hotelSystem.Reservation;

import com.hotelSystem.Room.Room;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ReservationRepository extends JpaRepository<Reservation,Long> {
    Optional<Reservation> findById(long id);
}
