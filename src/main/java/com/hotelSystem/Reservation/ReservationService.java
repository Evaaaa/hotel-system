package com.hotelSystem.Reservation;

import com.hotelSystem.exception.ResourceValidationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import javax.transaction.Transactional;
import java.time.LocalDate;

public class ReservationService {
    private final ReservationRepository repository;

    public ReservationService(ReservationRepository repository) {
        this.repository = repository;
    }


    public ReservationDTO create(final ReservationRequest request) {
        var isPresentReservation = repository.findById(request.id).isPresent();
        if (isPresentReservation) {
            throw new RuntimeException("The reservation already exists.");
        }
        var reservation = new Reservation(request.id);
        return repository.save(reservation).toDTO();
    }

    @Transactional
    public ReservationDTO update(final long reservationId, final ReservationRequest request) {
        var reservation = repository.findById(reservationId)
                .orElseThrow(() -> new ResourceValidationException("Reservation with ID " + reservationId + " is not found"));
        reservation.id = request.id;
        return repository.save(reservation).toDTO();
    }

    public ResponseEntity<HttpStatus> delete(final long reservationId) {
        var reservation = repository.findById(reservationId)
                .orElseThrow(() -> new ResourceValidationException("Reservation with ID " + reservationId + " is not found"));
        repository.delete(reservation);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    public ReservationDTO findById(final long reservationId) {
        var foundReservation = repository.findById(reservationId)
                .orElseThrow(() -> new RuntimeException("Reservation with ID " + reservationId + " is not found."));
        return new ReservationDTO( foundReservation.id,foundReservation.startDate,
                foundReservation.nights, foundReservation.allowedPeople, foundReservation.room);
    }
}
