package com.hotelSystem.Reservation;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/hotel/room/reservations")
public class ReservationResource {
    private final ReservationService reservationService;

    public ReservationResource(ReservationService reservationService) {
        this.reservationService = reservationService;
    }


    @PostMapping(consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ReservationDTO create(@RequestBody ReservationRequest request){
        return reservationService.create(request);
    }

    @PutMapping(path = "/{reservationId}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ReservationDTO update(@PathVariable("reservationId") final long reservationId, @RequestBody final ReservationRequest request){
        return reservationService.update(reservationId,request);
    }
    @DeleteMapping(path = "/{reservationId}",produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ResponseEntity<HttpStatus> delete(@PathVariable("reservationId") final long reservationId){
        return reservationService.delete(reservationId);
    }
    @GetMapping(path = "/find-by-id/{reservationId}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ReservationDTO findById(@PathVariable("reservationId") final long reservationId){
        return reservationService.findById(reservationId);
    }
}
