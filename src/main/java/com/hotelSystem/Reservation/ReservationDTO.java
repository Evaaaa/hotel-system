package com.hotelSystem.Reservation;

import com.hotelSystem.Room.Room;

import java.time.LocalDate;

public class ReservationDTO {
    private long id;
    private LocalDate startDate;
    private int nights;
    private int allowedPeople;
    private Room room = null;


    public ReservationDTO(long id,LocalDate startDate, int nights, int allowedPeople, Room room) {
        this.id = id;
        this.startDate = startDate;
        this.nights = nights;
        this.allowedPeople = allowedPeople;
        this.room = room;
    }
}
