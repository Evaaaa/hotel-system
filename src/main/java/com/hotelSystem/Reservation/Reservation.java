package com.hotelSystem.Reservation;

import com.hotelSystem.Room.Room;


import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "reservation")
public class Reservation {
    public long id;
    public LocalDate startDate;
    public int nights;
    public int allowedPeople;
    public Room room = null;

    public Reservation() {
    }

    public Reservation(long id, LocalDate startDate, int nights, int allowedPeople, Room room) {
        this.id = id;
        this.startDate = startDate;
        this.nights = nights;
        this.allowedPeople = allowedPeople;
        this.room = room;
    }

    public Reservation(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Reservation{" +
                ", startDate=" + startDate +
                ", nights=" + nights +
                ", numberOfPeople=" + allowedPeople +
                ", room=" + room.getId() +
                '}';


    }

    @Override
    public int hashCode() {

        return Objects.hash(31);
    }

    public long getId() {
        return id;

    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public ReservationDTO toDTO(){
        return new ReservationDTO(id, startDate, nights, allowedPeople, room);

}

}