package com.hotelSystem.config;

import com.hotelSystem.exception.ResourceValidationException;
import com.hotelSystem.util.ErrorDTO;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;



@ControllerAdvice
@Component
public class GlobalExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class.getName());

    @ExceptionHandler
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDTO handle(ResourceValidationException exception){
        LOGGER.error(exception.getMessage(), exception);
        return new ErrorDTO(exception.getMessage());
    }
    @ExceptionHandler
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDTO handle(ConstraintViolationException exception){
        LOGGER.error(exception.getMessage(), exception);
        return new ErrorDTO(exception.getMessage());
    }
    @ExceptionHandler
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorDTO handle(Exception exception){
        LOGGER.error(exception.getMessage(), exception);
        return new ErrorDTO(exception.getMessage());
    }
}
