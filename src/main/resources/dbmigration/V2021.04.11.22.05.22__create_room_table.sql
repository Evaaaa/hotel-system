CREATE TABLE room(
    id serial PRIMARY KEY,
	type character varying(20),
	numberOfPeople integer NOT NULL,
	price numeric(6,2) NOT NULL,
	hotelId integer NOT NULL,
CONSTRAINT fk_room_hotel FOREIGN KEY (hotelId) REFERENCES hotel (id)

);