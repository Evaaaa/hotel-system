CREATE TABLE reservation(
    id serial PRIMARY KEY,
	roomId integer NOT NULL,
	start_date date NOT NULL,
	end_date date NOT NULL,
	nights integer NOT NULL,
	number_of_people integer NOT NULL,
CONSTRAINT fk_reservation_room FOREIGN KEY (roomId) REFERENCES room (id)


);